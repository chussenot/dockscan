# frozen_string_literal: true

module Dockscan
  module Scan
    class Issue
      attr_accessor :severity, :description, :solution, :title, :tags, :references, :risk, :reflinks

      def initialize
        severity = 0
        description = 'Forgot to add description'
        solution = 'Information only.'
        title = 'Forgot to add title'
        tags = {}
        references = {}
        risk = {}
        reflinks = {}
      end
    end # Issue
  end # Scan
end # Dockscan
