# frozen_string_literal: true

class ReportStdout < Dockscan::Modules::ReportModule
  def info
    'This plugin produces brief stdout reports'
  end

  def format
    'stdout'
  end

  def file_extension
    '-stdout.txt'
  end

  def report(_opts)
    output = ''
    output << "Dockscan Report\n\n"

    issues = sortvulns
    7.downto(3) do |sev|
      next unless issues.key?(sev)

      output << sev2word(sev) << "\n"
      issues[sev].each do |v|
        # output << k << ": "
        output << v.vuln.title << ': '
        output << v.vuln.solution
        # output << v.output
        output << "\n"
      end
      output << "\n"
    end
    output
  end
end
