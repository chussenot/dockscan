# frozen_string_literal: true

class ReportText < Dockscan::Modules::ReportModule
  def info
    'This plugin produces text reports'
  end

  def format
    'txt'
  end

  def file_extension
    '.txt'
  end

  def report(_opts)
    output = ''
    output << "Dockscan Report\n\n"

    issues = sortvulns
    7.downto(3) do |sev|
      next unless issues.key?(sev)

      output << '-[ ' << sev2word(sev) << " ]-\n"
      issues[sev].each do |v|
        output << '=' << v.vuln.title << "=\n"
        output << "Description:\n" << v.vuln.description << "\n"
        output << "Output:\n" << v.output << "\n"
        output << "Solution:\n" << v.vuln.solution << "\n"
        output << "\n"
      end
      output << "\n"
    end
    output
  end
end
