# frozen_string_literal: true

class ContainerFileSystemShadow < Dockscan::Modules::AuditModule
  def info
    'This plugin checks /etc/shadow for problems'
  end

  def check(_dockercheck)
    sp = Dockscan::Scan::Plugin.new
    si = Dockscan::Scan::Issue.new
    si.title = 'Container have passwordless users in shadow'
    si.description = "Container have vulnerable entries in /etc/shadow.\nIt allows attacker to login or switch to user without password."
    si.solution = 'It is recommended to set password for user or to lock user account.'
    si.severity = 6 # High
    si.risk = { 'cvss' => 7.5 }
    sp.vuln = si
    sp.output = ''
    if scandata.key?('GetContainers') && !scandata['GetContainers'].obj.empty?
      sp.state = 'run'
      scandata['GetContainers'].obj.each do |container|
        content = ''
        container.copy('/etc/shadow') { |chunk| content += chunk }
        shcontent = ''
        Gem::Package::TarReader.new(StringIO.new(content)) { |t| shcontent = t.first.read }
        # shcontent.split("\n").each do |line|
        shcontent.lines.map(&:chomp).each do |line|
          shfield = line.split(':')
          if shfield[1].to_s == ''
            sp.state = 'vulnerable'
            sp.output << idcontainer(container) << " does not have password set for user: #{shfield[0]}\n"
          end
        end
      end
    end
    sp
  end
end
