# frozen_string_literal: true

module Dockscan
  module Modules
    class ReportModule < GenericModule
      attr_accessor :scandata

      def info
        raise "#{self.class.name} doesn't implement `handle_command`!"
      end

      def desc(item)
        item.vuln.description || false
      end

      def sortvulns
        severity_sorted = {}
        scandata.each do |_classname, scanissue|
          if (scanissue.state == 'vulnerable') || (scanissue.state == 'info')
            severity_sorted[scanissue.vuln.severity] ||= []
            severity_sorted[scanissue.vuln.severity] << scanissue
          end
        end
        severity_sorted
      end

      def format
        'unknown'
      end

      def file_extension
        '.unknown'
      end

      def getkey(hsh, hkey)
        if hsh.key?(hkey)
          hsh[hkey]
        else
          ''
        end
      end

      def sev2word(sev)
        case sev
        when  7
          'Critical'
        when  6
          'High'
        when  5
          'Medium'
        when  4
          'Low'
        when  3
          'Info'
        when  2
          'Verbose'
        when  1
          'Inspect'
        when  0
          'Debug'
        else
          'DebugMiss'
        end
      end

      def report(_opts)
        raise "#{self.class.name} doesn't implement `handle_command`!"
      end
  end # Class
    end # Module
end # Dockscan
