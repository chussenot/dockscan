# frozen_string_literal: true

require 'docker'

class GetDockerInfo < Dockscan::Modules::DiscoverModule
  def info
    'Info discovery module'
  end

  def run
    sp = Dockscan::Scan::Plugin.new
    sp.obj = Docker.info
    sp.output = sp.obj.map { |k, v| "#{k}=#{v}" }.join("\n")
    sp.state = 'run'
    sp
  end
end
