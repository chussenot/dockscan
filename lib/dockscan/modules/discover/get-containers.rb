# frozen_string_literal: true

class GetContainers < Dockscan::Modules::DiscoverModule
  def info
    'Container discovery module'
  end

  def run
    sp = Dockscan::Scan::Plugin.new
    sp.obj = Docker::Container.all(all: true)
    sp.output = sp.obj.map { |k, v| "#{k}=#{v}" }.join("\n")
    sp
  end
end
