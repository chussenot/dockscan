# frozen_string_literal: true

class GetDockerVersion < Dockscan::Modules::DiscoverModule
  def info
    'Info discovery module'
  end

  def run
    sp = Dockscan::Scan::Plugin.new
    sp.obj = Docker.version
    sp.output = sp.obj.map { |k, v| "#{k}=#{v}" }.join("\n")
    sp
  end
end
